#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

sub usage()
{
    print "Usage: network-metrics\n";
    exit(1);
}
my $optlist   = "dnb";
my $debug     = 0;
my $impotent  = 0;
my $daemon    = 0;
my $LOGFILE   = "/var/tmp/network-metrics.log";
my $PROXYIP   = "192.168.2.1";

# Daemon stuff
use libtestbed;

#
# Turn off line buffering on output
#
$| = 1;

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"b"})) {
    $daemon++;
}
usage()
    if (@ARGV);

my $INFLUXDB_HOSTNAME    = $ENV{"INFLUXDB_HOSTNAME"};
my $INFLUXDB_USERNAME    = $ENV{"INFLUXDB_USERNAME"};
my $INFLUXDB_PASSWORD    = $ENV{"INFLUXDB_PASSWORD"};
my $INFLUXDB_DATABASE    = $ENV{"INFLUXDB_DATABASE"};
my $INFLUXDB_GLOBAL_TAGS = $ENV{"INFLUXDB_GLOBAL_TAGS"};
if (! (defined($INFLUXDB_HOSTNAME) &&
       defined($INFLUXDB_USERNAME) &&
       defined($INFLUXDB_PASSWORD) &&
       defined($INFLUXDB_GLOBAL_TAGS) &&
       defined($INFLUXDB_DATABASE))) {
    die("Must define the influxdb environment variables\n");
}
$INFLUXDB_USERNAME = "metrics";

sub RunPing()
{
    my $command = "/bin/ping -i 2 -W 1 -O -n $PROXYIP";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	die("popen");
    }
    if ($pid) {
	while (<PIPE>) {
	    print $_;
	    if ($_ =~ / time=([\d\.]+) ms/) {
		UploadPingMetric($1);
	    }
	}
	close(PIPE);
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
}

sub UploadPingMetric($)
{
    my ($latency) = @_;
    
    my $url = "http://${INFLUXDB_HOSTNAME}:8086/write";
    $url .= "?db=${INFLUXDB_DATABASE} --data-binary ".
	"'ping,${INFLUXDB_GLOBAL_TAGS} latency=$latency'";

    my $command =
	"curl -m 10 -q -XPOST -u ${INFLUXDB_USERNAME}:${INFLUXDB_PASSWORD} $url";

    print "$command\n";
    if (!$impotent) {
	system($command);
    }
}

if ($daemon) {
    if (TBBackGround($LOGFILE)) {
	exit(0);
    }
}

#
# Loop, just keep restarting the ping. 
#
while (1) {
    RunPing();
    sleep(5);
}
