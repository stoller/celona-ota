#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

sub usage()
{
    print "Usage: attach-detach [-d]\n";
    exit(1);
}
my $optlist   = "cd";
my $TTYDEV    = "/dev/ttyUSB2";
my $CHAT      = "chat -t 1 -sv '' AT OK";
my $ATTACHCMD = "$CHAT 'AT+CFUN=1' OK < $TTYDEV > $TTYDEV";
my $DETACHCMD = "$CHAT 'AT+CFUN=4' OK < $TTYDEV > $TTYDEV";
my $PLMNCLEAR = "$CHAT 'AT+CRSM=214,28539,0,0,12,".
    "\\\"FFFFFFFFFFFFFFFFFFFFFFFF\\\"' OK < $TTYDEV > $TTYDEV";

#
# Turn off line buffering on output
#
$| = 1;

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
usage()
    if (@ARGV);

sub RunChat($)
{
    my ($chatcmd)  = @_;
    my $command = "sudo /bin/sh -c \"$chatcmd\"";
    my $output  = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	die("popen");
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
	print STDERR $output;
	if ($?) {
	    return undef;
	}
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}

if (defined($options{"d"})) {
    RunChat("$DETACHCMD");
}
elsif (defined($options{"c"})) {
    RunChat("$PLMNCLEAR");
}
else {
    RunChat("$ATTACHCMD");
}
exit(0);
