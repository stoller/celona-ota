"""Celona OTA workflow

Instructions:
  - [The Grafana web interface](http://{host-grafana}:3000/) (user `admin`, password `{password-perExptPassword}`)

"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum
# Route specific extensions.
import geni.rspec.emulab.route as route

IMAGEURN   = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
SHVLANNAME = "CEGCAP"

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

# Individual buses.
buses = [
    ('', "None"),
    ('urn:publicid:IDN+bus-test2.powderwireless.net+authority+cm',
     "Bus Test2"),
    ('urn:publicid:IDN+bus-build.powderwireless.net+authority+cm',
     "Bus Build"),
    ('urn:publicid:IDN+bus-4208.powderwireless.net+authority+cm',
     "Bus 4208"),
    ('urn:publicid:IDN+bus-4329.powderwireless.net+authority+cm',
     "Bus 4329"),
    ('urn:publicid:IDN+bus-4330.powderwireless.net+authority+cm',
     "Bus 4330"),
    ('urn:publicid:IDN+bus-4407.powderwireless.net+authority+cm',
     "Bus 4407"),
    ('urn:publicid:IDN+bus-4408.powderwireless.net+authority+cm',
     "Bus 4408"),
    ('urn:publicid:IDN+bus-4409.powderwireless.net+authority+cm',
     "Bus 4409"),
    ('urn:publicid:IDN+bus-4410.powderwireless.net+authority+cm',
     "Bus 4410"),
    ('urn:publicid:IDN+bus-4555.powderwireless.net+authority+cm',
     "Bus 4555"),
    ('urn:publicid:IDN+bus-4603.powderwireless.net+authority+cm',
     "Bus 4603"),
    ('urn:publicid:IDN+bus-4604.powderwireless.net+authority+cm',
     "Bus 4604"),
    ('urn:publicid:IDN+bus-4734.powderwireless.net+authority+cm',
     "Bus 4734"),
    ('urn:publicid:IDN+bus-4817.powderwireless.net+authority+cm',
     "Bus 4817"),
    ('urn:publicid:IDN+bus-4964.powderwireless.net+authority+cm',
     "Bus 4964"),
    ('urn:publicid:IDN+bus-5175.powderwireless.net+authority+cm',
     "Bus 5175"),
    ('urn:publicid:IDN+bus-6180.powderwireless.net+authority+cm',
     "Bus 6180"),
    ('urn:publicid:IDN+bus-6181.powderwireless.net+authority+cm',
     "Bus 6181"),
    ('urn:publicid:IDN+bus-6182.powderwireless.net+authority+cm',
     "Bus 6182"),
    ('urn:publicid:IDN+bus-6183.powderwireless.net+authority+cm',
     "Bus 6183"),
    ('urn:publicid:IDN+bus-6185.powderwireless.net+authority+cm',
     "Bus 6185"),
    ('urn:publicid:IDN+bus-6186.powderwireless.net+authority+cm',
     "Bus 6186")]

# A list of routes
routes = [
    ('',                      "None"),
    ('allroutes',             "All"),
    ('Circulator',            'Circulator'),
    ('Blue Detour',           'Blue Detour'),
    ('Orange',                'Orange'),
    ('SunnySide',             'SunnySide'),
    ('Wasatch Express',       'Wasatch Express'),
    ('Shoreline-U Hospital',  'Shoreline-U Hospital'),
    ('BSB-U Hospital',        'BSB-U Hospital'),
    ('HCI-Shoreline',         'HCI-Shoreline')
]

# Request a route
pc.defineParameter("Routes", "Bus Route",
                   portal.ParameterType.STRING, [routes[0][0]], routes,
                   min=1, multiValue=1, itemDefaultValue='')

# Request a specific bus
pc.defineParameter("Buses", "Bus",
                   portal.ParameterType.STRING, [buses[0][0]], buses,
                   min=1, multiValue=1, itemDefaultValue='')

pc.defineParameter("csoapikey",  "CSO API Token",
                   portal.ParameterType.STRING, "",
                   longDescription="CSO API Key, which will be encrypted.")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

#
# Individual buses
#
for bus in params.Buses:
    if bus != "":
        # Add a raw PC to the request
        name = findName(buses, bus).replace(" ", "-")
        node = request.RawPC(name)
        node.component_id = "ed1"
        node.component_manager_id = bus
        node.disk_image = IMAGEURN
        node.addService(pg.Execute(shell="bash", command="/local/repository/start-ue.pl -s"))
        node.startVNC()
        pass
    pass

#
# Or routes
#
for route in params.Routes:
    if route != "":
        busroute = request.requestBusRoute(route)
        busroute.disk_image = IMAGEURN
        busroute.addService(pg.Execute(shell="bash", command="/local/repository/start-ue.pl -s"))
        pass
    pass

con = request.RawPC("grafana")
con.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
con.hardware_type = "d430"
con.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
con.addService(pg.Execute(shell="bash", command="/local/repository/start-grafana.pl -s"))
con.startVNC()

# Hook up to the shared vlan with the proxy
lan1 = request.LAN("lan1")
lan1.vlan_tagging = False
lan1.setNoBandwidthShaping()
lan1.connectSharedVlan(SHVLANNAME)

iface = con.addInterface()
iface.addAddress(pg.IPv4Address("192.168.2.128", "255.255.255.0"))
lan1.addInterface(iface)

# Password for grafana, influx, etc.
request.addResource(ig.Password("perExptPassword"))

# CSO API Token
if params.csoapikey != "":
    request.addResource(ig.EncryptedBlock("CSO-Token-Encrypted",
                                          params.csoapikey))
    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
