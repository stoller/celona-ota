#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

sub usage()
{
    print "Usage: setup-ue.pl\n";
    exit(1);
}
my $optlist   = "sSR";
my $start     = 0;
my $TMPDIR    = "/var/tmp";
my $IPERFIP   = "192.168.2.128";

# Need to figure these out. 
my $INFLUXDB_HOSTNAME;
my $INFLUXDB_PASSWORD;
my $INFLUXDB_GLOBAL_TAGS;
my $CSO_APIKEY;
# Maybe not a good place for this. 
my $CSO_CUSTOMERID = "1000016324";

# Protos
sub StartGrafana();

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"s"})) {
    $start++;
}
if (defined($options{"S"}) || defined($options{"R"})) {
    StartGrafana();
    exit(0);
}
usage()
    if (@ARGV);

my $HOST = `hostname`;
chomp($HOST);
if ($HOST =~ /^([^\.]+)\.(.*)$/) {
    $HOST = $1;
    $INFLUXDB_HOSTNAME = "grafana.${2}";
}
else {
    die("Could not determine grafana hostname");
}

#
# Install from here.
#
if (! -e "/etc/grafana/grafana.ini") {
    if (! -e "/local/repository/install-ue.sh") {
	die("/local/repository/install-grafana.sh not found");
    }
    system("/local/repository/install-grafana.sh");
    if ($?) {
	die("Could not install grafana");
    }
}

$INFLUXDB_PASSWORD = `/bin/cat ${TMPDIR}/decrypted_admin_pass`;
if ($?) {
    die("Could not find influx password password");
}
chomp($INFLUXDB_PASSWORD);

#
# The CSO information. 
#
$CSO_APIKEY = `/bin/cat ${TMPDIR}/decrypted_csokey`;
if ($?) {
    die("Could not find CSO key");
}
chomp($CSO_APIKEY);

#
# Create the env variables file that the scripts use.
#
if (open(VARS, "> $TMPDIR/ue.env")) {
    print VARS "export INFLUXDB_HOSTNAME=\"${INFLUXDB_HOSTNAME}\"\n";
    print VARS "export INFLUXDB_USERNAME=\"metrics\"\n";
    print VARS "export INFLUXDB_PASSWORD=\"${INFLUXDB_PASSWORD}\"\n";
    print VARS "export INFLUXDB_BUCKET=\"metrics\"\n";
    print VARS "export INFLUXDB_ORG=\"metrics\"\n";
    print VARS "export INFLUXDB_DATABASE=\"metrics\"\n";
    print VARS "export CSO_CUSTOMERID=\"${CSO_CUSTOMERID}\"\n";
    print VARS "export CSO_APIKEY=\"${CSO_APIKEY}\"\n";

    #
    # And add them to our environment.
    #
    $ENV{"INFLUXDB_HOSTNAME"}    = ${INFLUXDB_HOSTNAME};
    $ENV{"INFLUXDB_USERNAME"}    = "metrics";
    $ENV{"INFLUXDB_PASSWORD"}    = ${INFLUXDB_PASSWORD};
    $ENV{"INFLUXDB_BUCKET"}      = "metrics";
    $ENV{"INFLUXDB_ORG"}         = "metrics";
    $ENV{"INFLUXDB_DATABASE"}    = "metrics";
    $ENV{"CSO_CUSTOMERID"}       = ${CSO_CUSTOMERID};
    $ENV{"CSO_APIKEY"}           = ${CSO_APIKEY};
}
else {
    die("Could not open $TMPDIR/ue.env for writing: $!");
}

if ($start) {
    StartGrafana();
}
exit(0);

sub StartGrafana()
{
    my @allbuses = (4410, 6186, 6185, 6183, 6182, 6181, 6180, 5175, 4964,
		 4817, 4734, 4604, 4603, 4555, 4410, 4409, 4407);

    # Command line override
    my @buses = (@ARGV ? @ARGV : @allbuses);
    
    #
    # iperf servers. One for every possible bus. Silly, I know. 
    #
    foreach my $bus (@buses) {
	if (! grep {$_ eq $bus} @allbuses) {
	    die("No such bus $bus in the bus list\n");
	}
	my $port = 50000 + $bus;
	my $pidfile = "/var/tmp/iperf-${bus}.pid";

	if (defined($options{"R"})) {
	    if (-e $pidfile) {
		my $pid = `/bin/cat $pidfile`;
		chomp($pid);
		system("sudo kill $pid");
		sleep(1);
	    }
	}
	print "Starting iperf server for bus $bus on port $port\n";
	my $command =
	    "daemon -N -r -n iperf-${bus} --delay=15 ".
	    "   --output=/var/tmp/iperf-${bus}.log ".
	    "   --pidfile=$pidfile -- ".
	    " /usr/local/bin/iperf3 -s -p $port -B $IPERFIP --forceflush ".
	    "   --rcv-timeout 30000 --snd-timeout 30000 ";
	#print "$command\n";
	system($command);
	if ($?) {
	    die("Could not start iperf server for bus $bus on port $port");
	}
	sleep(1);
    }
}
