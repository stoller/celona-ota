#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

sub usage()
{
    print "Usage: ue-metrics\n";
    exit(1);
}
my $optlist   = "dnb";
my $debug     = 0;
my $impotent  = 0;
my $daemon    = 0;
my $IMSI;
my $HOST;
my $LOGFILE   = "/var/log/ue-metrics.log";
my $TTYDEV    = "/dev/ttyUSB2";
my $CHAT      = "chat -t 1 -sv '' AT OK";
my $IMSIINFO  = 'AT+CIMI';
my $IMSICMD   = "$CHAT '${IMSIINFO}' OK < $TTYDEV > $TTYDEV";
my $CELLINFO  = 'AT+QENG=\"servingcell\"';
my $CELLCMD   = "$CHAT '${CELLINFO}' OK < $TTYDEV > $TTYDEV";

# Daemon stuff
use libtestbed;

#
# Turn off line buffering on output
#
$| = 1;

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"b"})) {
    $daemon++;
}
usage()
    if (@ARGV);

$HOST = `hostname`;
chomp($HOST);
if ($HOST =~ /(bus-\d+)/) {
    $HOST = $1;
}
else {
    $HOST = `/bin/cat /var/emulab/boot/nodeid`;
    chomp($HOST);
}
print "Host is $HOST\n";

#
# Easier this way.
#
my $INFLUXDB_HOSTNAME = $ENV{"INFLUXDB_HOSTNAME"};
my $INFLUXDB_USERNAME = $ENV{"INFLUXDB_USERNAME"};
my $INFLUXDB_PASSWORD = $ENV{"INFLUXDB_PASSWORD"};
my $INFLUXDB_DATABASE = $ENV{"INFLUXDB_DATABASE"};
if (! (defined($INFLUXDB_HOSTNAME) &&
       defined($INFLUXDB_USERNAME) &&
       defined($INFLUXDB_PASSWORD) &&
       defined($INFLUXDB_DATABASE))) {
    die("Must define the influxdb environment variables\n");
}

sub RunChat($)
{
    my ($chatcmd)  = @_;
    my $command = "sudo /bin/sh -c \"$chatcmd\"";
    my $output  = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	die("popen");
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
	if ($?) {
	    print STDERR $output;
	    return undef;
	}
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}

sub GetCellInfo()
{
    my $output = RunChat("$CELLCMD");
    if (!defined($output)) {
	return undef;
    }
    my @lines = split(/\n/, $output);
    while (@lines) {
	my $line = shift(@lines);
	if ($line =~ /NOCONN/) {
	    $line .= shift(@lines);
	    my @tokens = split(",", $line);
	    my $RSRP = $tokens[12];
	    my $RSRQ = $tokens[13];
	    my $SINR = $tokens[14];
	    return "RSRP=$RSRP,RSRQ=$RSRQ,SINR=$SINR";
	}
    }
    return undef;
}

sub UploadCellinfo($)
{
    my ($cellinfo) = @_;
    
    my $url = "http://${INFLUXDB_HOSTNAME}:8086/write";
    $url .= "?db=${INFLUXDB_DATABASE} --data-binary ".
	"'uemetrics,host=${HOST},imsi=${IMSI} $cellinfo'";

    my $command =
	"curl -m 10 -q -XPOST -u ${INFLUXDB_USERNAME}:${INFLUXDB_PASSWORD} $url";

    print "$command\n";
    if (!$impotent) {
	system($command);
    }
}

sub GetIMSI()
{
    my $output = RunChat("$IMSICMD");
    if (!defined($output)) {
	return undef;
    }
    my @lines = split(/\n/, $output);
    while (@lines) {
	my $line = shift(@lines);
	if ($line =~ /^(\d+)/) {
	    return $1;
	}
    }
    return undef;
}

$IMSI = GetIMSI();
if (!defined($IMSI)) {
    die("Could not get the UEs IMSI");
}
print "IMSI is $IMSI\n";

if ($daemon) {
    if (CheckDaemonRunning("ue-metrics")) {
	fatal("Not starting another metrics daemon!");
    }
    if (TBBackGround($LOGFILE)) {
	exit(0);
    }
    if (MarkDaemonRunning("ue-metrics")) {
	fatal("Could not mark daemon as running!");
    }

}

while (1) {
    sleep(10);
    my $cellinfo = GetCellInfo();
    if (!defined($cellinfo)) {
	print "Could not cell info, must still be detached\n";
	next;
    }
    print "$cellinfo\n";
    UploadCellinfo($cellinfo);
}
