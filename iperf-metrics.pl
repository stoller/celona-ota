#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use File::Tail;
use POSIX;

BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

sub usage()
{
    print "Usage: network-metrics\n";
    exit(1);
}
my $optlist   = "dnb";
my $debug     = 0;
my $impotent  = 0;
my $daemon    = 0;
my $LOGFILE   = "/var/tmp/iperf-metrics.log";
my $PROXYIP   = "192.168.2.1";
my $IPERFIP   = "192.168.2.128";
my $PORT      = 56789;

# Daemon stuff
use libtestbed;

#
# Turn off line buffering on output
#
$| = 1;

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"b"})) {
    $daemon++;
}
usage()
    if (@ARGV);

my $INFLUXDB_HOSTNAME    = $ENV{"INFLUXDB_HOSTNAME"};
my $INFLUXDB_USERNAME    = $ENV{"INFLUXDB_USERNAME"};
my $INFLUXDB_PASSWORD    = $ENV{"INFLUXDB_PASSWORD"};
my $INFLUXDB_DATABASE    = $ENV{"INFLUXDB_DATABASE"};
my $INFLUXDB_GLOBAL_TAGS = $ENV{"INFLUXDB_GLOBAL_TAGS"};
if (! (defined($INFLUXDB_HOSTNAME) &&
       defined($INFLUXDB_USERNAME) &&
       defined($INFLUXDB_PASSWORD) &&
       defined($INFLUXDB_GLOBAL_TAGS) &&
       defined($INFLUXDB_DATABASE))) {
    die("Must define the influxdb environment variables\n");
}
$INFLUXDB_USERNAME = "metrics";

#
# Oh, cheesy. The bus number plus 30000 is out port number. :-)
#
my $HOST = `hostname`;
if ($HOST =~ /bus\-(\d+)/) {
    $PORT = 50000 + $1;
}

sub RuniPerf()
{
    my $stop = 0;

    #
    # iperf likes to exit. So fork to continuously run it. If it exits, delay
    # and start it again. 
    #
    my $command = "/bin/iperf3 -R -p $PORT -i 2 -t 0 -b5M -l 1300 ".
	"--format M --logfile /tmp/iperf.log -c $IPERFIP";
    print "$command\n";
    
    system("/bin/rm -f /tmp/iperf.log")
	if (-e "/tmp/iperf.log");
    
    my $pid = fork();
    if ($pid) {
	sleep(1);
	return $pid;
    }
    while (!$stop) {
	print "Starting a new iperf\n";
	my $childpid = fork();
	if ($childpid) {
	    local $SIG{TERM} = sub { $stop = 1; kill("TERM", $childpid); };
	    local $SIG{INT}  = sub { $stop = 1; kill("TERM", $childpid); };
	    waitpid($childpid, 0);
	    print "iperf exited with $?\n";
	}
	else {
	    STDOUT->autoflush(1);
	    STDERR->autoflush(1);
	    open(STDERR, ">&STDOUT");
	    exec($command);
	}
	last
	    if ($stop);
	sleep(5);
    }
    exit(0);
}

sub ReadIPerfData()
{
    my $counter = 0;
    
    while (! -e "/tmp/iperf.log") {
	print "Waiting for log file to appear\n";
	sleep(1);
    }
    my $file = File::Tail->new("name" => "/tmp/iperf.log",
			       "maxinterval" => 1,
			       "interval"    => 1,
			       "adjustafter" => 5,
			       "reset_tail"  => 0);
    while (my $line = $file->read()) {
	print "$line";
	if ($line =~ /\s([\d\.]+) (\w+)\s+([\d\.]+) (\w+)\/sec/) {
	    print "$1, $2, $3, $4\n";
	    my $bw     = $1;
	    my $bunits = $2;
	    my $rate   = $3;
	    my $runits = $4;

	    if ($bw == 0 && $rate == 0) {
		$counter++;

		if ($counter > 30) {
		    print "No iperf data for too long\n";
		    $file->CLOSE();
		    return;
		}
	    }
	    $counter == 0;
	    if ($bw ne 0) {
		if ($bunits eq "Bytes") {
		    $bw = $bw / 1000000;
		}
		if ($bunits eq "KBytes") {
		    $bw = $bw / 1000;
		}
	    }
	    if ($rate ne 0) {
		if ($runits eq "Bytes") {
		    $rate = $rate / 1000000;
		}
		if ($runits eq "KBytes") {
		    $rate = $rate / 1000;
		}
	    }
	    UploadiPerfMetric($bw, $rate);
	}
    }
}

sub UploadiPerfMetric($)
{
    my ($bw, $bitrate) = @_;
    
    my $url = "http://${INFLUXDB_HOSTNAME}:8086/write";
    $url .= "?db=${INFLUXDB_DATABASE} --data-binary ".
	"'iperf,${INFLUXDB_GLOBAL_TAGS} bw=$bw,bitrate=$bitrate'";

    my $command =
	"curl -m 10 -q -XPOST -u ${INFLUXDB_USERNAME}:${INFLUXDB_PASSWORD} $url";

    #print "$command\n";
    if (!$impotent) {
	system($command);
    }
}

if ($daemon) {
    if (TBBackGround($LOGFILE)) {
	exit(0);
    }
}

#
# Loop, just keep restarting the ping. 
#
while (1) {
    my $childpid = RuniPerf();
    local $SIG{TERM} = sub { kill("TERM", $childpid); exit(0); };
    local $SIG{INT}  = sub { kill("TERM", $childpid); exit(0); };
    ReadIPerfData();
    kill("TERM", $childpid);
    waitpid($childpid, 0);
    sleep(1);
    print "Restarting iperf\n";
}

