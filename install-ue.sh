#!/bin/sh
set -x

SCRIPTNAME=$0
TMPDIR="/var/tmp"

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi

# The manifest holds the encrypted randomly generated password.
if [ ! -e $TMPDIR/manifest.xml ]; then
    geni-get manifest > $TMPDIR/manifest.xml
    cat $TMPDIR/manifest.xml | grep -q emulab:password
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get manifest failed or no password block!"
	exit 1
    fi
fi

# Geni key decrypts the password.
if [ ! -e $TMPDIR/geni.key ]; then
    geni-get key > $TMPDIR/geni.key
    cat $TMPDIR/geni.key | grep -q END\ .\*\PRIVATE\ KEY
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get key failed!"
	exit 1
    fi
fi

# Suck out the block from the manifest
if [ ! -e $TMPDIR/encrypted_admin_pass ]; then
    cat $TMPDIR/manifest.xml | perl -e '@lines = <STDIN>; $all = join("",@lines); if ($all =~ /^.+<[^:]+:password\s+name=\"perExptPassword\"[^>]*>([^<]+)<\/[^:]+:password>.+/igs) { print $1; }' > $TMPDIR/encrypted_admin_pass
fi

# And decrypt to get the password in plain text.
if [ ! -e $TMPDIR/decrypted_admin_pass -a -s $TMPDIR/encrypted_admin_pass ]; then
    openssl smime -decrypt -inform PEM -inkey $TMPDIR/geni.key -in $TMPDIR/encrypted_admin_pass -out $TMPDIR/decrypted_admin_pass
    if [ $? -ne 0 ]; then
	echo "ERROR: openssl decrypt password failed!"
	exit 1
    fi
fi
password=`/bin/cat $TMPDIR/decrypted_admin_pass`
if [ $? -ne 0 -o -z "$password" ]; then
    echo "ERROR: could not read decrypted password from file."
    exit 1
fi

sudo apt update
sudo apt-get -y install --no-install-recommends \
     libtimedate-perl libjson-perl gpsd-clients pip daemon iperf3 libfile-tail-perl
if [ $? -ne 0 ]; then
    echo 'ERROR: apt-install utilities failed'
    exit 1
fi

sudo pip install influxdb-client pyserial pytz
if [ $? -ne 0 ]; then
    echo 'ERROR: pip install install failed'
    exit 1
fi

# So we get the routes setup when we attach.
sudo cp /local/repository/udhcpc.script /etc/udhcpc/default.script
sudo chmod +x /etc/udhcpc/default.script

# marker
sudo touch /etc/.ue-installed

