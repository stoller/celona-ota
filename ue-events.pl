#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;
# This needs to be installed.
use JSON;
# libtimedate-perl
use Date::Parse;

BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

sub usage()
{
    print "Usage: ue-events\n";
    exit(1);
}
my $optlist   = "dnb";
my $debug     = 0;
my $impotent  = 0;
my $daemon    = 0;
my $CURL      = "/bin/curl -m 10";
my $LOGFILE   = "/var/log/ue-events.log";

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"b"})) {
    $daemon++;
}
usage()
    if (@ARGV);

use libtestbed;

#
# Turn off line buffering on output
#
$| = 1;

#
# Easier this way.
#
my $INFLUXDB_HOSTNAME = $ENV{"INFLUXDB_HOSTNAME"};
my $INFLUXDB_USERNAME = $ENV{"INFLUXDB_USERNAME"};
my $INFLUXDB_PASSWORD = $ENV{"INFLUXDB_PASSWORD"};
my $INFLUXDB_ORG      = $ENV{"INFLUXDB_ORG"};
my $INFLUXDB_DATABASE = $ENV{"INFLUXDB_DATABASE"};
if (! (defined($INFLUXDB_HOSTNAME) &&
       defined($INFLUXDB_USERNAME) &&
       defined($INFLUXDB_PASSWORD) &&
       defined($INFLUXDB_ORG) &&
       defined($INFLUXDB_DATABASE))) {
    die("Must define the influxdb environment variables\n");
}

#
# The Celona CSO stuff
#
my $CSO_APIKEY      = $ENV{"CSO_APIKEY"};
my $CSO_CUSTOMERID  = $ENV{"CSO_CUSTOMERID"};
if (! (defined($CSO_APIKEY) &&
       defined($CSO_CUSTOMERID))) {
    die("Must define the CSO environment variables\n");
}
my $CSO_STATSAPI    = "https://cso.celona.io/v1/api/stats";

#
# Need to handle multiple IMSIs (UEs). Store the latest timestamp uploaded.
#
my %IMSIs = ();

sub GetEvents($)
{
    my ($start) = @_;
    my $end     = time();
    
    my $url   = "${CSO_STATSAPI}/events/device?customerId=${CSO_CUSTOMERID}".
	"&startTime=${start}000&endTime=${end}000";
    my $command = "$CURL -s '$url' -H 'X-API-Key: ${CSO_APIKEY}'";
    #print "$command\n";

    my $data = "";
    
    if (open(CURL, "$command |")) {
	while (<CURL>) {
	    $data .= $_;
	}
	if (!close(CURL)) {
	    return undef;
	}
    }
    else {
	print STDERR "Could not get UE events\n";
	return undef;
    }
    my $tmp  = eval { decode_json($data) };
    if ($@) {
	print STDERR "Could not decode json data\n";
	return undef;
    }
    print Dumper($tmp);
    return $tmp;
}

sub UploadEvent($$)
{
    my ($imsi, $event) = @_;
    
    my $url = "'http://${INFLUXDB_HOSTNAME}:8086/write";
    $url .= "?db=${INFLUXDB_DATABASE}&precision=ms' --data-binary ";

    my $name  = $event->{"eventName"};
    my $host  = $event->{"deviceName"};
    my $stamp = $event->{"timestamp"};

    print "$imsi, $name, $stamp\n";
    
    my $q = "ueevents,imsi=${imsi},host=${host} eventType=\"$name\" $stamp";
    my $command = "$CURL -XPOST ".
	"-u ${INFLUXDB_USERNAME}:${INFLUXDB_PASSWORD} $url '$q'";

    print "$command\n";
    if (!$impotent) {
	system($command);
	if ($?) {
	    return -1;
	}
    }
    return 0;
}

#
# When starting up, we want to know the latest even so we do not end
# up duplicating. We need to do this for all of the IMSIs. 
#
sub GetMaxUploaded()
{
    my $url = "'http://${INFLUXDB_HOSTNAME}:8086/query";
    $url .= "?db=${INFLUXDB_DATABASE}&org=${INFLUXDB_ORG}' --data-urlencode ";

    my $q = "select last(eventType),time,imsi from ueevents ".
	"group by imsi order by time desc";

    my $command = "$CURL -s -XPOST ".
	"-u ${INFLUXDB_USERNAME}:${INFLUXDB_PASSWORD} $url \"q=$q\"";
    print "$command\n";

    my $data = "";
    
    if (open(CURL, "$command |")) {
	while (<CURL>) {
	    $data .= $_;
	}
	if (!close(CURL)) {
	    die("GetMaxUploaded: curl failed");
	}
    }
    else {
	die("GetMaxUploaded: could not start curl.");
    }
    my $tmp  = eval { decode_json($data) };
    if ($@) {
	print STDERR "Could not decode json data\n";
	next;
    }
    print Dumper($tmp);

    if (exists($tmp->{'results'}) &&
	exists($tmp->{'results'}->[0]->{'series'})) {
	my $series = $tmp->{'results'}->[0]->{'series'};

	foreach my $ref (@{$series}) {
	    my $stamp = $ref->{'values'}->[0]->[0];
	    my $time  = str2time($stamp);
	    my $imsi  = $ref->{'values'}->[0]->[2];

	    # Bizarre.
	    if ("$time" =~ /\./) {
		$time = $time * 1000;
	    }

	    next
		if ($imsi =~ /\"/);

	    if (!exists($IMSIs{$imsi}) || $time > $IMSIs{$imsi}) {
		$IMSIs{$imsi} = $time;
	    }
	}
    }
    foreach my $imsi (keys(%IMSIs)) {
	my $time = $IMSIs{$imsi};

	print "Max uploaded: $imsi, $time\n";
    }
}

# This will seed the most recently uploaded timestamp for any devices.
GetMaxUploaded();

if ($daemon) {
    if (CheckDaemonRunning("ue-events")) {
	fatal("Not starting another events daemon!");
    }
    if (TBBackGround($LOGFILE)) {
	exit(0);
    }
    if (MarkDaemonRunning("ue-events")) {
	fatal("Could not mark daemon as running!");
    }

}

while (1) {
    print Dumper(\%IMSIs);

    my $events = GetEvents(time() - (3600 * 1));
    goto again
	if (!defined($events));
    
    foreach my $ref (@{$events}) {
	my $imsi = $ref->{'imsi'};
	next
	    if (!defined($imsi));
	
	my $name = $ref->{'deviceName'};
	next
	    if ($name =~ /test/);
	
	my $stamp  = $ref->{"timestamp"};
	my $latest = $IMSIs{$imsi};

	next
	    if (defined($latest) && $stamp <= $latest);
	
	if (UploadEvent($imsi, $ref) == 0) {
	    if (!$impotent) {
		$IMSIs{$imsi} = $stamp;
		select(undef, undef, undef, 0.25);
	    }
	}
    }
  again:
    sleep(30);
}
