"""Setup Grafana for the Celona OTA workflow

Instructions:
  - [The Grafana web interface](http://{host-grafana}:3000/) (user `admin`, password `{password-perExptPassword}`)

"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum
# Route specific extensions.
import geni.rspec.emulab.route as route

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

pc.defineParameter("nodetype", "Node Type",
                   portal.ParameterType.STRING, "",
                   longDescription="Node type")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

con = request.RawPC("grafana")
con.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
if params.nodetype != "":
    con.hardware_type = params.nodetype
else:
    con.hardware_type = "d430"
    pass
con.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
con.startVNC()
con.addService(pg.Execute(shell="bash",
                          command="/local/repository/install-grafana.sh"))

# Password for grafana, influx, etc.
request.addResource(ig.Password("perExptPassword"))

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
