#!/usr/bin/python

import getopt
import os
import sys
import threading
import logging
import json
from influxdb_client import InfluxDBClient
from influxdb_client.client.write.point import Point
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime
import dateutil.parser
import traceback
import signal
import gps
import time

# Run in daemon mode when not debugging.
daemon = 0;

LOG = logging.getLogger(__name__)

def usage():
    print("Usage: " + sys.argv[0] + " -b")
    pass

class InfluxDBEnvConfig(object):

    def __init__(self,name):
        self.enabled = True
        self.name = name
        self.hostname = os.getenv("INFLUXDB_HOSTNAME","localhost")
        self.port = int(os.getenv("INFLUXDB_PORT",8086))
        self.username = os.getenv("INFLUXDB_USERNAME",None)
        self.password = os.getenv("INFLUXDB_PASSWORD",None)
        self.database = os.getenv("INFLUXDB_DATABASE","telegraf")
        self.token = os.getenv("INFLUXDB_TOKEN",None)
        self.bucket = os.getenv("INFLUXDB_BUCKET",None)
        self.org = os.getenv("INFLUXDB_ORG",None)
        self.ssl = os.getenv("INFLUXDB_SSL",False)
        if self.ssl in [1,"true","True","1"]:
            self.ssl = True
            self.url = "https://" + self.hostname + ":" + str(self.port)
        else:
            self.url = "http://" + self.hostname + ":" + str(self.port)
        self.measurement_prefix = os.getenv("INFLUXDB_MEASUREMENT_PREFIX","")
        self.global_tags = os.getenv("INFLUXDB_GLOBAL_TAGS",{})
        if self.global_tags:
            td = dict()
            for kvp in self.global_tags.split(","):
                (k,v) = kvp.split("=",1)
                td[k] = v
            self.global_tags = td

class GpsPipeEnvConfig(object):

    def __init__(self):
        self.hostname = os.getenv("GPSPIPE_HOSTNAME","localhost")
        self.port = int(os.getenv("GPSPIPE_PORT","2947"))
        if not self.hostname or not self.port:
            raise Exception("must specify both GPSPIPE_HOSTNAME and GPSPIPE_PORT")

    def to_dict(self):
        return dict(hostname=self.hostname,port=self.port)

class InfluxDBLogger(threading.Thread):
    def __init__(self,outputs=[]):
        super(InfluxDBLogger,self).__init__()
        self.clients = dict()
        self.configs = dict()
        self.queues = dict()
        self.lock = threading.Lock()
        self.cv = threading.Condition(lock=self.lock)
        self._cancel = False

        for output in outputs:
            if not output.enabled:
                continue
            if output.name in self.clients:
                raise ConfigError("duplicate influxdb output name %s" % (output.name,))

            client = InfluxDBClient(
                output.url, timeout=2000, org=output.org,
                username=output.username,password=output.password,
                token=output.token)
            self.clients[output.name] = client.write_api(write_options=SYNCHRONOUS)
            self.configs[output.name] = output
            self.queues[output.name] = []

    def log(self,points=[]):
        if not points:
            return
        if not self.is_alive():
            self.start()
        for name in self.clients:
            newpoints = []
            for p in points:
                np = dict()
                if self.configs[name].measurement_prefix:
                    ms = "%s%s" % (self.configs[name].measurement_prefix,
                                   p["measurement"])
                else:
                    ms = p["measurement"]
                np = dict(measurement=ms)
                if "time" in p:
                    np["time"] = p["time"]
                if "tags" not in p:
                    np["tags"] = dict()
                else:
                    np["tags"] = p["tags"]
                for (t,v) in self.configs[name].global_tags.items():
                    p["tags"][t] = v
                if "fields" not in p:
                    np["fields"] = dict()
                else:
                    np["fields"] = p["fields"]
                newpoints.append(Point.from_dict(np))
            self.lock.acquire()
            self.queues[name].extend(newpoints)
            self.lock.release()
        with self.cv:
            self.cv.notify()

    def run(self):
        tel = []
        try:
            import urllib3.exceptions
            tel.append(urllib3.exceptions.ReadTimeoutError)
        except:
            pass
        try:
            import requests.exceptions
            tel.append(requests.exceptions.ReadTimeout)
        except:
            pass
        if not tel:
            tel = [Exception]
        tel = tuple(tel)
        while True:
            if self._cancel:
                LOG.debug("stopping influxdb event log thread")
                return
            tosend = dict()
            sent = dict()
            remaining = 0
            self.cv.acquire()
            for name in self.queues.keys():
                if len(self.queues[name]):
                    tosend[name] = min(len(self.queues[name]),32)
                    if len(self.queues[name]) > 32:
                        remaining += len(self.queues[name]) - 32
            self.cv.release()
            if tosend:
                for name in tosend.keys():
                    try:
                        config = self.configs[name]
                        self.clients[name].write(
                            config.bucket, config.org, 
                            self.queues[name][0:tosend[name]])
                        sent[name] = tosend[name]
                        LOG.debug("sent %d points to output %s",tosend[name],name)
                    except tel:
                        LOG.warn("timeout while sending %d points to output %s",
                                  tosend[name],name)
                        remaining += tosend[name]
                    except Exception:
                        LOG.error("failed to send %d points to output %s",
                                  tosend[name],name,exc_info=sys.exc_info())
                        remaining += tosend[name]
            if sent:
                self.cv.acquire()
                for name in sent.keys():
                    del self.queues[name][0:sent[name]]
                self.cv.release()
            timeout = None
            if remaining:
                timeout = 8
            with self.cv:
                self.cv.wait(timeout=timeout)

    def close(self):
        LOG.debug("closing influxdb event log")
        self._cancel = True
        with self.cv:
            self.cv.notify()

class GpsPipeEventReader(object):

    def __init__(self,logger,hostname="localhost",port="2947"):
        self.logger = logger
        self.hostname = hostname
        self.port = int(port)
        self.session = None

    def run(self):
        if self.session:
            try:
                self.session.close()
            except:
                pass
        self.session = gps.gps(host=self.hostname,port=self.port)
        self.session.stream(flags=gps.WATCH_JSON)
        LOG.info("connected to gpsd at %s:%s, waiting for data..." % (
            self.hostname,self.port))
        for j in self.session:
            if not self.session.valid:
                print("session no longer valid, maybe the control node turned off");
                time.sleep(1)
                return
            if "class" not in j:
                continue
            points = []
            if j["class"] == "TPV":
                LOG.debug("gpsd tpv: " + repr(j))
                point = dict(measurement="gpsd_tpv",tags={},fields={})
                if "time" not in j:
                    point["time"] = datetime.datetime.utcnow()
                else:
                    d = dateutil.parser.parse(j["time"])
                    point["time"] = d
                    #if getattr(d,'timestamp',None):
                    #    point["time"] = int(d.timestamp()*(10**9))
                    #else:
                    #    point["time"] = int((d - datetime.datetime(1970,1,1,tzinfo=d.tzinfo)).total_seconds()*(10**9))
                    #point["time"] = int(point["time"] - d.utcoffset().total_seconds())
                for k in j.keys():
                    kd = k.encode("utf8")
                    if kd.decode() in [ "class","device","mode","time","satellites" ]:
                        continue
                    point["fields"][kd] = j[k]
                if point["fields"]:
                    points.append(point)
            elif j["class"] == "SKY":
                LOG.debug("gpsd sky: " + repr(j))
                point = dict(measurement="gpsd_sky",tags={},fields={})
                if "time" not in j:
                    point["time"] = datetime.datetime.utcnow()
                else:
                    point["time"] = dateutil.parser.parse(j["time"])
                for k in j.keys():
                    kd = k.encode("utf8")
                    if kd.decode() in [ "class","device","time","satellites" ]:
                        continue
                    point["fields"][k] = j[k]
                if "satellites" in j:
                    point["fields"]["satellites"] = 0
                    point["fields"]["satellites_used"] = 0
                    for sat in j["satellites"]:
                        point["fields"]["satellites"] += 1
                        if "used" in sat and sat["used"]:
                            point["fields"]["satellites_used"] += 1
                if point["fields"]:
                    points.append(point)
            self.logger.log(points)

def sigh(signo,frame):
    ilog.close()
    exit(0)

if __name__ == "__main__":
    #
    # Process program arguments.
    # 
    try:
        opts, req_args =  getopt.getopt(sys.argv[1:], "hb", ["help", "daemon"])
        for opt, val in opts:
            if opt in ("-h", "--help"):
                usage()
                sys.exit()
                pass
            elif opt in ("-b", "--daemon"):
                daemon = 1;
                pass
            pass
        pass
    except getopt.error as e:
        print(e.args[0])
        usage()
        sys.exit(2)
        pass

    signal.signal(signal.SIGINT,sigh)
    signal.signal(signal.SIGHUP,sigh)
    signal.signal(signal.SIGTERM,sigh)

    try:
        import urllib3
        urllib3.disable_warnings()
    except:
        pass

    gc = GpsPipeEnvConfig()
    ic = InfluxDBEnvConfig("overwatch")

    if daemon:
        try:
            fp = open("/var/tmp/gpspipe-to-influx.log", "a");
            sys.stdout = fp
            sys.stderr = fp
            sys.stdin.close();
            pass
        except:
            print("Could not open log file for append")
            sys.exit(1);
            pass
        
        pid = os.fork()
        if pid:
            sys.exit(0)
            pass
        os.setsid();
        pass

    logging.basicConfig()
    if os.getenv("GPSPIPE_TO_INFLUX_DEBUG","0") == "1":
        LOG.setLevel(logging.DEBUG)

    ilog = InfluxDBLogger(outputs=[ic])
    gpr = GpsPipeEventReader(ilog,hostname=gc.hostname,port=gc.port)

    try:
        ilog.start()
    except:
        traceback.print_exc()
        exit(1)
    while True:
        try:
            gpr.run()
        except:
            traceback.print_exc()
            ilog.close()
            exit(1)
    ilog.close()
    exit(1)
