"""Celona OTA workflow

Instructions:
  - [The Grafana web interface](http://{host-grafana}:3000/) (user `admin`, password `{password-perExptPassword}`)

"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum
# Route specific extensions.
import geni.rspec.emulab.route as route

EBC        = "urn:publicid:IDN+ebc.powderwireless.net+authority+cm"
IMAGEURN   = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
SHVLANNAME = "CEGCAP"

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

pc.defineParameter("csoapikey",  "CSO API Token",
                   portal.ParameterType.STRING, "",
                   longDescription="CSO API Key, which will be encrypted.")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

ebc = request.RawPC("ebc-nu1")
ebc.component_manager_id = EBC
ebc.component_id = "nuc1"
ebc.disk_image = IMAGEURN
ebc.startVNC()
ebc.addService(pg.Execute(shell="bash", command="/local/repository/start-ue.pl"))

con = request.RawPC("grafana")
con.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
con.hardware_type = "d430"
con.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
con.addService(pg.Execute(shell="bash", command="/local/repository/start-grafana.pl"))
con.startVNC()

# Hook up to the shared vlan with the proxy
lan1 = request.LAN("lan1")
lan1.vlan_tagging = False
lan1.setNoBandwidthShaping()
lan1.connectSharedVlan(SHVLANNAME)

iface = con.addInterface()
iface.addAddress(pg.IPv4Address("192.168.2.128", "255.255.255.0"))
lan1.addInterface(iface)

# Password for grafana, influx, etc.
request.addResource(ig.Password("perExptPassword"))

# CSO API Token
if params.csoapikey != "":
    request.addResource(ig.EncryptedBlock("CSO-Token-Encrypted",
                                          params.csoapikey))
    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
