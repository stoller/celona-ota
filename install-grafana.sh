#!/bin/sh
set -x

SCRIPTNAME=$0
TMPDIR="/var/tmp"

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi

# The manifest holds the encrypted randomly generated password.
if [ ! -e $TMPDIR/manifest.xml ]; then
    geni-get manifest > $TMPDIR/manifest.xml
    cat $TMPDIR/manifest.xml | grep -q emulab:password
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get manifest failed or no password block!"
	exit 1
    fi
fi

# Geni key decrypts the password.
if [ ! -e $TMPDIR/geni.key ]; then
    geni-get key > $TMPDIR/geni.key
    cat $TMPDIR/geni.key | grep -q END\ .\*\PRIVATE\ KEY
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get key failed!"
	exit 1
    fi
fi

# Suck out the key from the manifest
if [ ! -e $TMPDIR/encrypted_admin_pass ]; then
    cat $TMPDIR/manifest.xml | perl -e '@lines = <STDIN>; $all = join("",@lines); if ($all =~ /^.+<[^:]+:password\s+name=\"perExptPassword\"[^>]*>([^<]+)<\/[^:]+:password>.+/igs) { print $1; }' > $TMPDIR/encrypted_admin_pass
fi

# And decrypt to get the password in plain text.
if [ ! -e $TMPDIR/decrypted_admin_pass -a -s $TMPDIR/encrypted_admin_pass ]; then
    openssl smime -decrypt -inform PEM -inkey $TMPDIR/geni.key -in $TMPDIR/encrypted_admin_pass -out $TMPDIR/decrypted_admin_pass
    if [ $? -ne 0 ]; then
	echo "ERROR: openssl decrypt failed!"
	exit 1
    fi
fi
password=`/bin/cat $TMPDIR/decrypted_admin_pass`
if [ $? -ne 0 -o -z "$password" ]; then
    echo "ERROR: could not read decrypted password from file."
    exit 1
fi

#
# The Celona CSO key is also encrypted. We need this for access to the CSO.
#
if [ ! -e $TMPDIR/encrypted_csokey ]; then
    cat $TMPDIR/manifest.xml | perl -e '@lines = <STDIN>; $all = join("",@lines); if ($all =~ /^.+<[^:]+:encrypt\s+name=\"CSO-Token-Encrypted\"[^>]*>([^<]+)<\/[^:]+:encrypt>.+/igs) { print $1; }' > $TMPDIR/encrypted_csokey
fi

# And decrypt to get the password in plain text.
if [ ! -e $TMPDIR/decrypted_csokey -a -s $TMPDIR/encrypted_csokey ]; then
    openssl smime -decrypt -inform PEM -inkey $TMPDIR/geni.key -in $TMPDIR/encrypted_csokey -out $TMPDIR/decrypted_csokey
    if [ $? -ne 0 ]; then
	echo "ERROR: openssl decrypt csokey failed!"
	exit 1
    fi
    csokey=`/bin/cat $TMPDIR/decrypted_csokey`
    if [ $? -ne 0 -o -z "$csokey" ]; then
	echo "ERROR: could not read decrypted cso from file."
	exit 1
    fi
fi

sudo apt update

#
# Modern version of influx.
#
wget --retry-connrefused --retry-on-host-error -O $TMPDIR/influxdata-archive_compat.key \
     https://repos.influxdata.com/influxdata-archive_compat.key

if [ ! -e /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg ]; then
    echo "393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c $TMPDIR/influxdata-archive_compat.key" | sha256sum -c && cat $TMPDIR/influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
    if [ $? -ne 0 ]; then
	echo "ERROR: creating /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg failed!"
	exit 1
    fi
fi

if [ ! -e /etc/apt/sources.list.d/influxdata.list ]; then
    echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
    if [ $? -ne 0 ]; then
	echo "ERROR: creating /etc/apt/sources.list.d/influxdata.list failed!"
	exit 1
    fi
fi    

sudo apt update
sudo apt-get -y install --no-install-recommends libtimedate-perl libjson-perl \
     libfile-tail-perl
if [ $? -ne 0 ]; then
    echo 'ERROR: install utilities failed'
    exit 1
fi

sudo apt-get -y install --no-install-recommends influxdb2 influxdb2-client
if [ $? -ne 0 ]; then
    echo 'ERROR: install influxdb2 failed'
    exit 1
fi
sudo systemctl enable influxdb
sudo systemctl start influxdb

#
# This is just for the web interface
#
influx org find -n metrics
if [ $? -ne 0 ]; then
    influx setup --username admin --password $password -org metrics --bucket metrics --force
    if [ $? -ne 0 ]; then
	echo 'ERROR: failed to setup influxdb admin user'
	exit 1
    fi
fi

#
# To create a DB password we need the bucket id (why not the name?).
#
bucketid=`influx bucket ls | grep metrics | awk '{print $1}'`
if [ $? -ne 0 -o -z "$bucketid" ]; then
    echo "ERROR: could not get the bucket id."
    exit 1
fi

#
# This user is for DB manipulation.
#
influx v1 auth find -o metrics --username metrics
if [ $? -ne 0 ]; then
    influx v1 auth create -o metrics --username metrics --password $password \
	   --write-bucket $bucketid --read-bucket $bucketid
    if [ $? -ne 0 ]; then
	echo 'ERROR: failed to setup influxdb database user'
	exit 1
    fi
fi

#
# And grafana.
#
if [ ! -e /etc/apt/trusted.gpg.d/grafana.gpg ]; then
    wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/grafana.gpg > /dev/null    
    if [ $? -ne 0 ]; then
	echo "ERROR: creating /etc/apt/trusted.gpg.d/grafana.gpg failed!"
	exit 1
    fi
fi    
    
if [ ! -e /etc/apt/sources.list.d/grafana.list ]; then
    echo "deb [signed-by=/etc/apt/trusted.gpg.d/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
    if [ $? -ne 0 ]; then
	echo "ERROR: creating /etc/apt/sources.list.d/grafana.list failed!"
	exit 1
    fi
fi
sudo apt-get update

sudo apt-get -y install --no-install-recommends grafana sqlite3 crudini pip daemon
if [ $? -ne 0 ]; then
    echo 'ERROR: install grafana failed'
    exit 1
fi

sudo crudini --set /etc/grafana/grafana.ini security admin_password $password
if [ $? -ne 0 ]; then
    echo 'ERROR: setting grafana password in /etc/grafana/grafana.ini failed'
    exit 1
fi

sudo grafana-cli --config /etc/grafana/grafana.ini admin reset-admin-password $password
if [ $? -ne 0 ]; then
    echo 'ERROR: setting grafana password with grafana-cli failed'
    exit 1
fi

#
# Install the Trackmap plugin.
#
sudo grafana-cli plugins install pr0ps-trackmap-panel
if [ $? -ne 0 ]; then
    echo 'ERROR: installing trackmap plugin failed'
    exit 1
fi
sudo chown -R grafana:grafana /var/lib/grafana
sudo chown -R grafana:grafana /var/log/grafana
sudo systemctl enable grafana-server
sudo systemctl start grafana-server

echo "Delaying so grafana can get started ..."
sleep 10

#
# Add the influx datasource
#
goo="{\"name\": \"InfluxDB2\",\"type\": \"influxdb\",\"uid\": \"OzcR1Jo4j\",\"url\": \"http://localhost:8086\",\"secureJsonFields\": {\"password\": true},\"user\": \"metrics\",\"database\": \"metrics\",\"isDefault\": true,\"editable\": true,\"basicAuth\":false,\"basicAuthUser\":\"\",\"basicAuthPassword\":\"\",\"withCredentials\": false,\"access\":\"proxy\",\"secureJsonData\":{\"password\": \"${password}\"}}"

curl -u admin:$password --header 'Accept: application/json' \
     --header 'Content-Type: application/json' \
     -d "$goo" http://localhost:3000/api/datasources
if [ $? -ne 0 ]; then
    echo 'ERROR: adding influxdb datasource to grafana failed'
    exit 1
fi

#
# The metrics dashboard.
#
dashboard="dashboard.json"
if [ ! -e $dashboard -a -e /local/repository/dashboard.json ]; then
    dashboard="/local/repository/dashboard.json"
fi
curl --header 'Accept: application/json' --header 'Content-Type: application/json' -u admin:$password -d @${dashboard} http://localhost:3000/api/dashboards/db
if [ $? -ne 0 ]; then
    echo 'ERROR: adding metrics dashboard to grafana failed'
    exit 1
fi

sudo pip install influxdb-client pyserial pytz
if [ $? -ne 0 ]; then
    echo 'ERROR: pip install install failed'
    exit 1
fi

#
# Latest version of iperf fixes the server side hang problem.
#
tar -C /var/tmp -zxf /local/repository/iperf-3.15.tar.gz
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not unpack iperf3 tarfile'
    exit 1
fi
(cd /var/tmp/iperf-3.15; ./configure; make)
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not configure/build iperf3'
    exit 1
fi
(cd /var/tmp/iperf-3.15; sudo make install)
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not install iperf3'
    exit 1
fi
