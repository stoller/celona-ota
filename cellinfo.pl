#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

sub usage()
{
    print "Usage: cellinfo.pl\n";
    exit(1);
}
my $optlist   = "";
my $TTYDEV    = "/dev/ttyUSB2";
my $CHAT      = "chat -t 3 -sv '' AT OK";
my $CELLINFO  = 'AT+QENG=servingcell';
#my $CELLINFO  = 'AT+QSCAN=2';
my $CELLCMD   = "$CHAT '${CELLINFO}' OK < $TTYDEV > $TTYDEV";

# Protos
sub RunChat($);
sub GetInfo();

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
usage()
    if (@ARGV);

my $info = GetInfo();
if (!defined($info)) {
    die("Could not get the UEs cell info");
}
print "$info\n";

exit(0);

sub RunChat($)
{
    my ($chatcmd)  = @_;
    my $command = "sudo /bin/sh -c \"$chatcmd\"";
    my $output  = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	die("popen");
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
	if ($?) {
	    print STDERR $output;
	    return undef;
	}
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}

sub GetInfo()
{
    my $output = RunChat("$CELLCMD");
    if (!defined($output)) {
	return undef;
    }
    return $output;
}
